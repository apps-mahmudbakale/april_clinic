<?php

namespace Database\Seeders;

use Jajo\NG;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;

class StateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $json = file_get_contents(database_path('json/state_lgas.json'));
        $data = json_decode($json, true);

        foreach ($data as $stateData) {
            $stateId = DB::table('states')->insertGetId([
                'name' => $stateData['state'],
                'created_at' => now()
            ]);

            foreach ($stateData['lgas'] as $lga) {
                DB::table('lgas')->insert([
                    'state_id' => $stateId,
                    'name' => $lga,
                    'created_at' => now()
                    // Add other LGA fields as needed
                ]);
            }
        }
    }
}
