@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="row">
        <div class="col-sm-12">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('app.roles.index')}}">Roles </a></li>
                <li class="breadcrumb-item"><i class="feather-chevron-right"></i></li>
                <li class="breadcrumb-item active">Roles Create</li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-body">
                <form action="{{route('app.roles.store')}}" method="POST">
                    @csrf
                    <div class="row">
                        <div class="col-12">
                            <div class="form-heading">
                                <h4>Role Details</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-xl-12">
                            <div class="input-block local-forms">
                                <label>Name <span class="login-danger">*</span></label>
                                <input class="form-control" type="text" name="name" placeholder="">
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-xl-12">
                            <div class="row">
                                @foreach ($permissions as $permission)
                                <div class="col-sm-3">
                                  <label class="form-check form-check-sm form-check-custom form-check-solid me-5 me-lg-20">
                                    <input class="form-check-input" type="checkbox" value="{{$permission->id}}" name="permissions[]">
                                    <span class="form-check-label">{{$permission->name}}</span>
                                  </label>
                                  <p></p>
                                </div>
                                @endforeach
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="doctor-submit text-end">
                                <button type="submit" class="btn btn-primary submit-form me-2">Submit</button>
                                {{-- <button type="submit" class="btn btn-primary cancel-form">Cancel</button> --}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
