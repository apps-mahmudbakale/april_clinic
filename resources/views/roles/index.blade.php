@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="row">
        <div class="col-sm-12">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('app.users.index')}}">Roles </a></li>
                <li class="breadcrumb-item"><i class="feather-chevron-right"></i></li>
                <li class="breadcrumb-item active">Roles List</li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">

        <div class="card card-table show-entire">
            <div class="card-body">

                <!-- Table Header -->
                <div class="page-table-header mb-2">
                    <div class="row align-items-center">
                        <div class="col">
                            <div class="doctor-table-blk">
                                <h3>Roles List</h3>
                                <div class="doctor-search-blk">
                                    <div class="top-nav-search table-search-blk">
                                        <form>
                                            <input type="text" class="form-control" placeholder="Search here">
                                            <a class="btn"><img src="{{asset('assets/img/icons/search-normal.svg')}}" alt=""></a>
                                        </form>
                                    </div>
                                    <div class="add-group">
                                        <a href="{{route('app.roles.create')}}" class="btn btn-primary add-pluss ms-2"><img src="{{asset('assets/img/icons/plus.svg')}}" alt=""></a>
                                        <a href="javascript:;" class="btn btn-primary doctor-refresh ms-2"><img src="{{asset('assets/img/icons/re-fresh.svg')}}" alt=""></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-auto text-end float-end ms-auto download-grp">
                            <a href="javascript:;" class=" me-2"><img src="{{asset('assets/img/icons/pdf-icon-01.svg')}}" alt=""></a>
                            <a href="javascript:;" class=" me-2"><img src="{{asset('assets/img/icons/pdf-icon-02.svg')}}" alt=""></a>
                            <a href="javascript:;" class=" me-2"><img src="{{asset('assets/img/icons/pdf-icon-03.svg')}}" alt=""></a>
                            <a href="javascript:;" ><img src="assets/img/icons/pdf-icon-04.svg" alt=""></a>

                        </div>
                    </div>
                </div>
                <!-- /Table Header -->

               <livewire:roles/>
            </div>
        </div>
    </div>
</div>
@endsection
