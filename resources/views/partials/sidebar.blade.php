<div class="sidebar" id="sidebar">
    <div class="sidebar-inner slimscroll">
        <div id="sidebar-menu" class="sidebar-menu">
            <ul>
                <li class="menu-title">Main</li>
                <li>
                    <a href="{{route('app.dashboard')}}"><span class="menu-side"><img src="{{asset('assets/img/icons/menu-icon-01.svg')}}" alt=""></span> <span> Dashboard </span></a>
                </li>
                <li class="submenu">
                    <a href="#" class="active subdrop"><span class="menu-side"><img src="{{asset('assets/img/icons/menu-icon-02.svg')}}" alt=""></span> <span> Users </span> <span class="menu-arrow"></span></a>
                    <ul style="display: block;">
                        <li><a href="{{route('app.users.index')}}">Users List</a></li>
                        <li><a href="{{route('app.roles.index')}}">Roles</a></li>
                    </ul>
                </li>
                <li>
                    <a href="{{route('app.doctors.index')}}"><span class="menu-side"><img src="{{asset('assets/img/icons/menu-icon-02.svg')}}" alt=""></span> <span> Doctors </span></a>
                </li>

            </ul>
            <div class="logout-btn">
                <a href="login.html"><span class="menu-side"><img src="{{asset('assets/img/icons/logout.svg')}}" alt=""></span> <span>Logout</span></a>
            </div>
        </div>
    </div>
</div>
