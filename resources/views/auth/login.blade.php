@extends('layouts.auth')

@section('content')
 <!-- Main Wrapper -->
 <div class="main-wrapper login-body">
    <div class="container-fluid px-0">
        <div class="row">

            <!-- Login logo -->
            <div class="col-lg-6 login-wrap">
                <div class="login-sec">
                    <div class="log-img">
                        <img class="img-fluid" src="assets/img/login-02.png" alt="Logo">
                    </div>
                </div>
            </div>
            <!-- /Login logo -->

            <!-- Login Content -->
            <div class="col-lg-6 login-wrap-bg">
                <div class="login-wrapper">
                    <div class="loginbox">
                        <div class="login-right">
                            <div class="login-right-wrap">
                                <div class="account-logo">
                                    <a href=""><h2>April Hospital</h2></a>
                                </div>
                                <h2>Login</h2>
                                <!-- Form -->
                                <form action="{{route('login')}}" method="POST">
                                    @csrf
                                    <div class="input-block">
                                        <label >Email <span class="login-danger">*</span></label>
                                        <input class="form-control" name="email" type="text" >
                                    </div>
                                    <div class="input-block">
                                        <label >Password <span class="login-danger">*</span></label>
                                        <input class="form-control pass-input" name="password" type="password" >
                                        <span class="profile-views feather-eye-off toggle-password"></span>
                                    </div>
                                    <div class="forgotpass">
                                        <div class="remember-me">
                                            <label class="custom_check mr-2 mb-0 d-inline-flex remember-me"> Remember me
                                            <input type="checkbox" name="radio">
                                            <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <a href="">Forgot Password?</a>
                                    </div>
                                    <div class="input-block login-btn">
                                        <button class="btn btn-primary btn-block" type="submit">Login</button>
                                    </div>
                                </form>
                                <!-- /Form -->

                                <div class="next-sign">
                                    <p class="account-subtitle">Need an account?  <a href="">Sign Up</a></p>


                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <!-- /Login Content -->

        </div>
    </div>
</div>
<!-- /Main Wrapper -->
@endsection
