@extends('layouts.app')

@section('content')
<div class="page-header">
    <div class="row">
        <div class="col-sm-12">
            <ul class="breadcrumb">
                <li class="breadcrumb-item"><a href="{{route('app.users.index')}}">Users </a></li>
                <li class="breadcrumb-item"><i class="feather-chevron-right"></i></li>
                <li class="breadcrumb-item active">Users Update</li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">

        <div class="card">
            <div class="card-body">
                <form action="{{route('app.users.update', $user->id)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <div class="col-12">
                            <div class="form-heading">
                                <h4>Users Details</h4>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-xl-6">
                            <div class="input-block local-forms">
                                <label>First Name <span class="login-danger">*</span></label>
                                <input class="form-control" type="text" name="firstname" value="{{old('firstname', isset($user) ? $user->firstname : '' )}}" placeholder="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-xl-6">
                            <div class="input-block local-forms">
                                <label>Last Name <span class="login-danger">*</span></label>
                                <input class="form-control" name="lastname" value="{{old('lastname', isset($user) ? $user->lastname : '' )}}" type="text" placeholder="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-xl-6">
                            <div class="input-block local-forms">
                                <label>Phone <span class="login-danger">*</span></label>
                                <input class="form-control" name="phone" value="{{old('phone', isset($user) ? $user->phone : '' )}}" type="text" placeholder="">
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-xl-6">
                            <div class="input-block local-forms">
                                <label>Email <span class="login-danger">*</span></label>
                                <input class="form-control" name="email" value="{{old('email', isset($user) ? $user->email : '' )}}" type="email" placeholder="">
                            </div>
                        </div>
                        <div class="col-12 col-md-12 col-xl-12">
                            <div class="input-block local-forms">
                                <label>Role <span class="login-danger">*</span></label>
                                <select name="role" id="" class="form-control">
                                    <option value="{{$user->roles()->first()->id}}">{{$user->roles()->first()->name}}</option>
                                    @foreach ($roles as $role)
                                        <option value="{{$role->id}}">{{$role->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="doctor-submit text-end">
                                <button type="submit" class="btn btn-primary submit-form me-2">Submit</button>
                                {{-- <button type="submit" class="btn btn-primary cancel-form">Cancel</button> --}}
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
