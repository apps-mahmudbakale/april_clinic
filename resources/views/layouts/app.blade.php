<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0">
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/favicon.png">
    <title>Preclinic - Medical & Hospital - Bootstrap 5 Admin Template</title>
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

	<!-- Bootstrap CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/bootstrap.min.css')}}">

	<!-- Fontawesome CSS -->
    <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome/css/fontawesome.min.css')}}">
    <link rel="stylesheet" href="{{asset('assets/plugins/fontawesome/css/all.min.css')}}">

	<!-- Select2 CSS -->
	<link rel="stylesheet" type="text/css" href="{{asset('assets/css/select2.min.css')}}">

	<!-- Datatables CSS -->
	<link rel="stylesheet" href="{{asset('assets/plugins/datatables/datatables.min.css')}}">

	<!-- Feathericon CSS -->
	<link rel="stylesheet" href="{{asset('assets/css/feather.css')}}">

	<!-- Main CSS -->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/css/style.css')}}">
    @livewireStyles
</head>
<body>
    <div class="main-wrapper">
        @include('partials.header')
        @include('partials.sidebar')
        <div class="page-wrapper">
            <div class="content">
    @yield('content')
            </div>
        </div>
    </div>
    @livewireScripts
    <script src="{{asset('assets/js/jquery-3.7.1.min.js')}}"></script>

	<!-- Bootstrap Core JS -->
    <script src="{{asset('assets/js/bootstrap.bundle.min.js')}}"></script>

	<!-- Feather Js -->
	<script src="{{asset('assets/js/feather.min.js')}}"></script>

	<!-- Slimscroll -->
    <script src="{{asset('assets/js/jquery.slimscroll.js')}}"></script>

	<!-- Select2 Js -->
	<script src="{{asset('assets/js/select2.min.js')}}"></script>

	<!-- Datatables JS -->
	<script src="{{asset('assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
	<script src="{{asset('assets/plugins/datatables/datatables.min.js')}}"></script>

	<!-- counterup JS -->
	<script src="{{asset('assets/js/jquery.waypoints.js')}}"></script>
	<script src="{{asset('assets/js/jquery.counterup.min.js')}}"></script>

	<!-- Apexchart JS -->
	<script src="{{asset('assets/plugins/apexchart/apexcharts.min.js')}}"></script>
	<script src="{{asset('assets/plugins/apexchart/chart-data.js')}}"></script>
    <script src="{{asset('assets/plugins/sweetalert/sweetalert2.all.min.js')}}"></script>
	<script src="{{asset('assets/plugins/sweetalert/sweetalerts.min.js')}}"></script>

	<!-- Custom JS -->
    <script src="{{asset('assets/js/app.js')}}"></script>
    <form id="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
        {{ csrf_field() }}
    </form>
    <script>
        @if (session()->has('success'))
            Swal.fire({
                title: 'Success!',
                text: '{{ session()->get('success') }} !',
                icon: 'success',
                customClass: {
                    confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
            });
        @elseif (session()->has('error'))
            Swal.fire({
                title: 'Error!',
                text: '{{ session()->get('error') }} !',
                icon: 'error',
                customClass: {
                    confirmButton: 'btn btn-primary'
                },
                buttonsStyling: false
            });
        @endif
    </script>
</body>
</html>
