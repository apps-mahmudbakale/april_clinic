<div>
    <div class="table-responsive">
        <table class="table border-0 custom-table comman-table datatable mb-0">
            <thead>
                <tr>
                    <th>S/N</th>
                    <th>Name</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($roles as $role)
                <tr>
                    <td>{{$loop->iteration}}</td>
                    <td>{{$role->name}}</td>
                    <td>
                        <div class="dropdown dropdown-action">
                            <a href="#" class="action-icon dropdown-toggle" data-bs-toggle="dropdown" aria-expanded="false"><i class="fa fa-ellipsis-v"></i></a>
                            <div class="dropdown-menu dropdown-menu-end">
                                <a class="dropdown-item" href="{{route('app.roles.edit', $role->id)}}"><i class="fa-solid fa-pen-to-square m-r-5"></i> Edit</a>
                                <a class="dropdown-item" id="delete{{ $role->id }}" data-value="{{ $role->id }}"><i class="fa fa-trash-alt m-r-5"></i> Delete</a>
                                <script>
                                    document.querySelector('#delete{{ $role->id }}').addEventListener('click', function(e) {
                                        // alert(this.getAttribute('data-value'));
                                        Swal.fire({
                                            title: 'Are you sure?',
                                            text: "You won't be able to revert this!",
                                            icon: 'warning',
                                            showCancelButton: true,
                                            confirmButtonColor: '#3085d6',
                                            cancelButtonColor: '#d33',
                                            confirmButtonText: 'Yes, delete it!'
                                        }).then((result) => {
                                            if (result.isConfirmed) {
                                                document.getElementById('delete#'+this.getAttribute('data-value')).submit();
                                            }
                                        })
                                    })
                                </script>
                                <form id="delete#{{ $role->id }}"
                                    action="{{ route('app.roles.destroy', $role->id) }}" method="POST"
                                     style="display: inline-block;">
                                    <input type="hidden" name="_method" value="DELETE">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                            </div>
                        </div>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
