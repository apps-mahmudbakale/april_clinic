<?php

namespace App\Livewire;

use App\Models\User;
use Livewire\Component;

class Doctors extends Component
{
    public function render()
    {
        $users = User::all();
        return view('livewire.doctors', ['users' => $users]);
    }
}
