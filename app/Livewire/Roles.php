<?php

namespace App\Livewire;

use App\Models\Role;
use Livewire\Component;

class Roles extends Component
{
    public function render()
    {
        $roles = Role::all();
        return view('livewire.roles', ['roles' => $roles]);
    }
}
